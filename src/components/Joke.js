import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

const Joke = ({ joke }) => {
  console.log("joke", joke);
  /*
  const [jokeStr, setJokeStr] = useState("nothing ...");
  useEffect(() => {
    if (typeof joke === "string") {
      setJokeStr(joke);
    } else {
      joke.then(function (str) {
        setJokeStr(str);
      });
    }
  }, [joke]);
  */
  return <p>Joke: {joke}</p>;
};

export default connect(state => ({ joke: state.joke }))(Joke);