import React from "react";
import { connect } from "react-redux";

const Increment = ({ incr }) => {
  return <button onClick={incr}>+</button>;
};

function mapDispatchToProps(dispatch) {
  return {
    incr: () => dispatch({ type: "counter/increment" }),
  };
}

export default connect(undefined, mapDispatchToProps)(Increment);