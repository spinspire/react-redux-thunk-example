import React from "react";
import { connect } from "react-redux";

const Counter = ({ count, date, reset }) => <>
  <p>
    Count: {count}
  </p>
  <p>Date: {date.toString()}</p>
  <button onClick={reset}>RESET</button>
</>;

function mapsStateProps(state) {
  return {
    count: state.counter,
    date: state.date,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    reset: () => dispatch({ type: "counter/reset" }),
  };
}
export default connect(mapsStateProps, mapDispatchToProps)(Counter);