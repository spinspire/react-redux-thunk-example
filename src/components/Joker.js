import React from "react";
import { connect } from "react-redux";

async function fetchJoke(dispatch) {
  const url = "http://api.icndb.com/jokes/random";
  dispatch({ type: "ajax/started" });
  await new Promise(resolve => {
    setTimeout(resolve, 3000);
  });
  const res = await fetch(url);
  const json = await res.json();
  dispatch({ type: "ajax/ended" });
  dispatch({ type: "joke/fetch", payload: json.value.joke });
}

function fetchJokeNoThunk(dispatch) {
  const url = "http://api.icndb.com/jokes/random";
  dispatch({ type: "ajax/started" });
  fetch(url)
    .then(res => res.json())
    .then(json => {
      dispatch({ type: "ajax/ended" });
      dispatch({ type: "joke/fetch", payload: json.value.joke });
    });
}

const Joker = ({ dispatch }) => {
  function tellJoke() {
    dispatch(fetchJoke);
  }
  return <button onClick={tellJoke}>Tell me a joke</button>;
};

export default connect()(Joker);