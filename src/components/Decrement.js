import React from "react";
import { connect } from "react-redux";

const Decrement = ({ incr }) => {
  return <button onClick={incr}>-</button>;
};

function mapDispatchToProps(dispatch) {
  return {
    incr: () => dispatch({ type: "counter/decrement" }),
  };
}

export default connect(undefined, mapDispatchToProps)(Decrement);