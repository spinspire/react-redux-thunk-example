import React from "react";
import { connect } from "react-redux";

const Spinner = ({ ajax }) => {
  return <p>
    Ajax: {ajax ? "FETCHING ..." : "DONE"}
  </p>;
};

export default connect(state => ({ ajax: state.ajax }))(Spinner);