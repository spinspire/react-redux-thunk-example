import { combineReducers } from "redux";
import counterReducer from "./counter/reducer";
import dateReducer from "./date/reducer";
import jokeReducer from "./joke/reducer";
import ajaxReducer from "./ajax/reducer";

const rootReducer = combineReducers({
  date: dateReducer,
  counter: counterReducer,
  joke: jokeReducer,
  ajax: ajaxReducer,
});

/*
export default (state, action) => {
  console.log("STATE BEFORE", state, action);
  state = rootReducer(state, action);
  console.log("STATE AFTER", state, action);
  return state;
};
*/
export default rootReducer;