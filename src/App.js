import React from 'react';
import logo from './logo.svg';
import './App.css';
// import Address from "./Address";
import Counter from "./components/Counter";
import Increment from "./components/Increment";
import Decrement from "./components/Decrement";
import Joke from './components/Joke';
import Joker from './components/Joker';
import Spinner from './components/Spinner';

function App() {
  return (
    <div className="App">
      <Counter />
      <Increment />
      <Decrement />
      <Spinner />
      <Joke />
      <Joker />
    </div>
  );
}

export default App;
