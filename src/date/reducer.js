export default function (state = new Date(), action) {
  switch (action.type) {
    case "date/set":
      return action.payload;
    case "date/today":
      return new Date();
    default:
  }
  return state;
}