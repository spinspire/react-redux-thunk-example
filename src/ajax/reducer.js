export default function (state = false, action) {
  switch (action.type) {
    case "ajax/started":
      return true;
    case "ajax/ended":
      return false;
    default:
  }
  return state;
}