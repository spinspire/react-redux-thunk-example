export default function (state = 0, action) {
  switch (action.type) {
    case "counter/increment":
      return ++state;
    case "counter/decrement":
      return --state;
    case "counter/reset":
      return 0;
    default:
  }
  return state;
}